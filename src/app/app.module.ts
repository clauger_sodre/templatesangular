import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CourseModule } from './courses/course-module';
import { CoreModule} from './courses/Core/core.module';
import { CardModule } from './material.module/card/cardModule';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormField } from './material.module/form/formModule';
import { CamposModule } from './shared/component/campos/input-text/campos.module';


@NgModule({
  declarations: [ // importa componentes
    AppComponent // componentes criados
   
  ],
  imports: [ // importa modulos
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CourseModule, // modulo segregado
    CoreModule,
    CardModule,
    FormField,
    CamposModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
      path: '', redirectTo: 'courses', pathMatch: 'full'
      }
    ]),
    BrowserAnimationsModule
  ],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'pt'}], // seta local para pt
  bootstrap: [AppComponent]
})
export class AppModule { }
