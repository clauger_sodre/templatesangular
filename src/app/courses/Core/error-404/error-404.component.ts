import { Component, OnInit } from "@angular/core";

@Component({
    templateUrl: './error-404.component.html'
})
export class Error404Component implements OnInit{
    caminhoImg404:string="";
    ngOnInit(): void {
        this.caminhoImg404= '/assets/images/img-404.png';
    }
 
}