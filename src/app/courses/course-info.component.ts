import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Course } from "./course";
import { CourseServices } from "./course.services";

@Component({
    templateUrl: './course-info.component.html'
})
export class CourseInfoComponent implements OnInit{

    //courseId!:number;
    course!: Course;
    //courseFind!: Course;
    constructor(private actvatedRoute: ActivatedRoute, private CourseService : CourseServices){

    }

    ngOnInit(): void {
      
           // this.courseId= +this.actvatedRoute.snapshot.paramMap.get('id')!;
        //this.courseFind = this.CourseService.retrieveById(this.courseId= +this.actvatedRoute.snapshot.paramMap.get('id')!);
        this.CourseService.retrieveById(+this.actvatedRoute.snapshot.paramMap.get('id')!).subscribe({
            next: course => this.course = course,
            error: err => console.log('Erro: ',err)
            
        });
           
    }
    save():void{
        //this.CourseService.save(this.courseFind);
        this.CourseService.save(this.course).subscribe({
            next: course => console.log('Saved course with success: ',course),
            error: err => console.log('Erro: ',err)
        });
    }
}

