import { Component, OnInit } from "@angular/core";
import { Course } from "./course";
import { CourseServices } from "./course.services";

@Component({
   
    templateUrl: './course-list-component.html'
})
export class CourseListComponent implements OnInit{
    filteredCourses: Course[]=[];
    _courses: Course[]= [];
   
   _filterBy: string ="" ;
    
    constructor (private CourseServices: CourseServices){

    }
    ngOnInit(): void {
      
        this.retrieveAll();
       
        // this._courses=this.CourseServices.retrieveAll();
        // this.filteredCourses = this._courses;
    }
    
    retrieveAll(): void{ //função assincrona para retornar a lista
        this.CourseServices.retrieveAll().subscribe({
            next: courses =>{
                this._courses = courses;
                this.filteredCourses = this._courses;
               
            },
            error: err => console.log('Erro: ',err)
        })
        
    };

    deleteById(courseId: number): void{
        this.CourseServices.deleteById(courseId).subscribe({//chama a função da classe CourseServices
            next: ()=>{ // não faz nada
                console.log("Deleted with sucess!")
                this.fetchData();// reload na pagina
            },
            error: err => console.log('Erro: ',err)
        })
    };

    fetchData() {
        this.CourseServices.retrieveAll().subscribe(courses => {
            this._courses = courses;
            this.filteredCourses = this._courses;
        });
    }
    set filter(value: string) { 
        this._filterBy = value;

        this.filteredCourses = this._courses.filter((course: Course) => course.name.toLocaleLowerCase().indexOf(this._filterBy.toLocaleLowerCase()) > -1);
    }

    get filter() { 
        return this._filterBy;
    }
}