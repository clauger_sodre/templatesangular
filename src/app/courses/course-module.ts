import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { StarModule } from "../shared/component/star/star.module";
import { CourseInfoComponent } from "./course-info.component";
import { CourseListComponent } from "./course-list.component";

import { PipeModule } from "../shared/pipe/pipeModule";
import { CardFancyExample } from "../material.module/card/card-fancy-example";
import { FormFieldAppearanceExample } from "../material.module/form/form-field";
import { DemoMaterialModule } from "../material.module/material.module";
import { FormField } from "../material.module/form/formModule";



@NgModule({
    declarations:[ // importa os componentes criados
        CourseListComponent,
        
        CourseInfoComponent,
        
    ],
    imports: [ // importa o modulos
        CommonModule,
        PipeModule, // importa o modulo criado
        StarModule, // importa o modulo criado
        FormsModule,
        RouterModule.forChild([
            {
                path: 'courses', component: CourseListComponent
              },
              {
                path: 'courses/info/:id', component: CourseInfoComponent
              },
              {
                path: 'card', component: CardFancyExample 
              },
              {
                path: 'criar', component: FormFieldAppearanceExample
              }
        ])
    ]
})
export class CourseModule {

}