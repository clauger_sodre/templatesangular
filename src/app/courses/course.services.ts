import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { Course } from "./course";

@Injectable({
    providedIn: 'root'
})
export class CourseServices{
   
   
    
    constructor(private httpClient: HttpClient){}//injeção de dependencia
    private courseUrl: string="http://localhost:3100/api/courses"; // endpoint onde será consumido 
  
   

    retrieveAll(): Observable <Course[]>{
        return this.httpClient.get<Course[]>(this.courseUrl);//retorna um observable
    }

    retrieveById(id: number): Observable<Course> {
    //    var cursoFiltrado: Course;
    //    cursoFiltrado = COURSES.find((courseIterator: Course) => courseIterator.id === id)!;
       
    //     return cursoFiltrado;
    if(id!=null || id!=""){
    return this.httpClient.get<Course>(`${this.courseUrl}/${id}`);
    }
    else{
   
       return  Observable.throw("Não encontrou nenhum curso com esse id");  
    }
    }
    save(course: Course): Observable<Course>{
        if(course.id){
            // const index = COURSES.findIndex((courseIterator: Course) => courseIterator.id === course.id);
            // COURSES[index]=course;
            return this.httpClient.put<Course>(`${this.courseUrl}/${course.id}`,course);
        }
        else{
            return this.httpClient.post<Course>(`${this.courseUrl}`,course);
        }
    }
    deleteById(id: number): Observable<any>{// sem retorno no backend ou retorna qualquer coisa
        return this.httpClient.delete<any>(`${this.courseUrl}/${id}`); // envia o metodo delete com o id
    }
   
}

var COURSES : Course[]=[
    {
        id: 1,
        name: 'joao',
        imageUrl: '/assets/images/1.jpg',
        price: 99.99,
        code: 1,
        duration: 120.0,
        rating: 3.4
    },
    {
        id: 2,
        name: 'maria',
        imageUrl: '/assets/images/2.jpg',
        price: 89.99,
        code: 2,
        duration: 80.0,
        rating: 4.4
    },
    {
        id: 3,
        name: 'JOSETPH',
        imageUrl: '/assets/images/1.jpg',
        price: 79.99,
        code: 3,
        duration: 83.0,
        rating: 4.7
    },
    {
        id: 4,
        name: 'Angular: Router',
        duration: 80,
        code: 5,
        rating: 4.5,
        price: 46.99,
        imageUrl: '/assets/images/router.png',
    },
    {
        id: 5,
        name: 'Angular: Animations',
        duration: 80,
        code: 6,
        rating: 5,
        price: 56.99,
        imageUrl: '/assets/images/animations.png',
    }

]

