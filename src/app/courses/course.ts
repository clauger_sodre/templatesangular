export class Course{
    id!: number;
    name: string= "maria";
    imageUrl!: string;
    price!: number;
    code!: number;
    duration!: number;
    rating!: number;
}