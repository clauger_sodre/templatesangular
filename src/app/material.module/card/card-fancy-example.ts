import {Component, OnInit} from '@angular/core';
import { Course } from 'src/app/courses/course';
import { CourseServices } from "../../courses/course.services";


/**
 * @title Card with multiple sections
 */
@Component({
  selector: 'card-fancy-example',
  templateUrl: 'card-fancy-example.html',
  styleUrls: ['card-fancy-example.css'],
})
export class CardFancyExample implements OnInit{
  filteredCourses: Course[]=[];
  _courses: Course[]= [];
  _filterBy: string ="" ;

  ngOnInit(): void {
    this.retrieveAll()
  }
  constructor (private CourseServices: CourseServices){ }

  retrieveAll(): void{ //função assincrona para retornar a lista
    this.CourseServices.retrieveAll().subscribe({
      next: courses =>{
          this._courses = courses;
          this.filteredCourses = this._courses;
          console.log("Cursos resgatados: ",this.filteredCourses)
      },
      error: err => console.log('Erro: ',err)
  })
  
    
};

fetchData() {
  this.CourseServices.retrieveAll().subscribe(courses => {
      this._courses = courses;
      this.filteredCourses = this._courses;
  });
}

deleteById(courseId: number): void{
  this.CourseServices.deleteById(courseId).subscribe({//chama a função da classe CourseServices
      next: ()=>{ // não faz nada
          console.log("Deleted with sucess!")
          this.fetchData();// reload na pagina
      },
      error: err => console.log('Erro: ',err)
  })
};

get filter() { 
  return this._filterBy;
}

set filter(value: string) { 
  this._filterBy = value;

  this.filteredCourses = this._courses.filter((course: Course) => course.name.toLocaleLowerCase().indexOf(this._filterBy.toLocaleLowerCase()) > -1);
}
}

