import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { CourseInfoComponent } from "src/app/courses/course-info.component";
import { DemoMaterialModule } from "../material.module";
import { CardFancyExample } from "./card-fancy-example";

@NgModule({
    declarations:[
        CardFancyExample 
    ],
    imports:[
        DemoMaterialModule,
        BrowserModule,
        RouterModule.forChild([
            
              {
                path: 'courses/info/:id', component: CourseInfoComponent
              }
        ])      
        
    ],
    exports:[
        CardFancyExample 
    ]
})
export class CardModule{

}