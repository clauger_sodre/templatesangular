import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Course } from 'src/app/courses/course';
import { CourseServices } from 'src/app/courses/course.services';



@Component({
  selector: 'form-field',
  templateUrl: './form-field.html'
})
export class FormFieldAppearanceExample implements OnInit{
  formCliente!: FormGroup;

  constructor(private formBuilder: FormBuilder, private CourseService : CourseServices) { }

  // para gerar os erros
  get f(){
    return this.formCliente.controls
  }
  ngOnInit() {
    this.formCliente = this.formBuilder.group({
      nome: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(256)]],
      id: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(8)]],
      imageUrl: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(256)]],
      code: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(10)]]
      
    });
 
  }

 

  onSubmit(): void {
    // aqui você pode implementar a logica para fazer seu formulário salvar
    if(this.formCliente.invalid){
      return
    }
    else{
      
    console.log(this.formCliente.value);
    let cursoFormulario: Course={
      id: 0,
      name: this.formCliente.controls['nome'].value,
      imageUrl: this.formCliente.controls['imageUrl'].value,
      price: 99.99,
      code: this.formCliente.controls['imageUrl'].value,
      duration: 120.0,
      rating: 3.4
    }
    alert('Sucesso\n\n'+ JSON.stringify(this.formCliente.value, null,4))
   
    //cursoFormulario.name = this.formCliente.controls['nome'].value;
    console.log("Novo curso", cursoFormulario);
    this.CourseService.save(cursoFormulario).subscribe({
      next: cursoFormulario => console.log('Saved course with success: ',cursoFormulario),
      error: err => console.log('Erro: ',err)
  });
    //save(cursoFormulario);
    }
  }  
  reset(): void{
     // Usar o método reset para limpar os controles na tela
     this.formCliente.reset();
  }
}