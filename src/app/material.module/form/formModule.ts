import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { CamposModule } from "src/app/shared/component/campos/input-text/campos.module";
import { DemoMaterialModule } from "../material.module";
import { FormFieldAppearanceExample } from "./form-field";

@NgModule({
    declarations:[
        FormFieldAppearanceExample 
    ],
    imports:[
        DemoMaterialModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule ,
        CamposModule
       
        
    ],
    exports:[
        FormFieldAppearanceExample 
    ]
})
export class FormField {
}