import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { DemoMaterialModule } from "../../../../../app/material.module/material.module";
import { InputTextComponent } from "./input-text.component";

@NgModule({
    declarations:[
        InputTextComponent
    ],
    imports:[
        CommonModule,
        DemoMaterialModule,
        FormsModule,
        ReactiveFormsModule         
    ],
    exports:[
        InputTextComponent
    ]
})
export class CamposModule {
}