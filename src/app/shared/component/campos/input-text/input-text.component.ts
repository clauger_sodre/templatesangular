import {  Component, Input, OnInit } from '@angular/core';
import { AbstractControl,  FormGroup } from '@angular/forms';

@Component({
  selector: 'meu-input-text',
  templateUrl: './input-text.component.html'
})

export class InputTextComponent implements OnInit {
  
  ngOnInit(): void {
   
  }

  @Input() titulo!: string;
  @Input() formGroup! : FormGroup;
  @Input() controlName!: string;


 get formControl(): AbstractControl {
   return this.formGroup.controls[this.controlName];
 }

}
