import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'replace'
})
export class ReplacePipe implements PipeTransform{
    transform(value: number) {
        let valuetToReplace: string;
        valuetToReplace = "R$ "+value+" doletas";
        return  valuetToReplace;
    }

    
}