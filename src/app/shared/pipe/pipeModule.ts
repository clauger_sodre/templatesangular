import { NgModule } from "@angular/core";
import { ReplacePipe } from "src/app/shared/pipe/ReplacePipe";

@NgModule({
    declarations:[
        ReplacePipe
    ],
    exports:[
        ReplacePipe
    ]
})
export class PipeModule{

}